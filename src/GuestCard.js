import React, { Component } from 'react'

class GuestCard extends Component {
    render = () => {
        const { guest, changeStatus } = this.props
        return (
            <li>
                <span className={guest.isAbsent ? 'present' : 'absent'}>{ guest.guest.name }</span>
                <input type="checkbox" checked={guest.isAbsent} onChange={changeStatus(guest.guest.index)}/>
            </li>
        )
    }
}

export default GuestCard