import './App.css';
import React, { Component } from 'react';
import GuestCard from './GuestCard';
import SearchActions from './SearchActions';
import guests from './guests.json';
const changedGuests = guests.map(guest => ({
    guest,
    isAbsent: false
}));

class App extends Component {
    state = {
        guests: changedGuests,
        fullSearchableGuests: [],
        searchResultPosition: 0,
        searchableGuests: [],
        search: ''
    }

    searchGuest = (e) => {
        const inputed = e.target.value.toLowerCase()
        const result = this.state.guests.filter(el => {
            for (let key in el.guest) {
                if(key === '_id') continue;
                if(Object.hasOwnProperty.call(el.guest, key) && (typeof el.guest[key] === 'string')) {
                    let info = el.guest[key].toLowerCase()
                    if(info.indexOf(inputed) !== -1) {
                        return true
                    }
                }
            }
            return false
        })

        const exp = result.length > 10 && inputed
        this.setState({
            search: e.target.value,
            searchResultPosition: exp > 10 ? 1 : 0,
            fullSearchableGuests: result,
            searchableGuests: exp ? result.slice(0, 10) : result
        })
    }

    addMoreFound = () => {
        const { searchResultPosition, searchableGuests, fullSearchableGuests } = this.state
        this.setState({
            searchResultPosition: searchResultPosition+1,
            searchableGuests: searchableGuests.concat(fullSearchableGuests.slice(searchResultPosition*10, searchResultPosition*10+10))
        })
    }

    showAllFound = () => {
        const { searchResultPosition, searchableGuests, fullSearchableGuests } = this.state
        this.setState({
            searchResultPosition: 0,
            searchableGuests: searchableGuests.concat(fullSearchableGuests.slice(searchResultPosition*10))
        })
    }

    changeGuestStatus = (guestId) => (e) => {
        const g = this.state.guests.map(el => {
            if(el.guest.index === guestId) {
                el.isAbsent = !el.isAbsent
            }
            return el
        })
        this.setState({
            guests: g
        })
    }

    render() {
        const { searchGuest, changeGuestStatus, addMoreFound, showAllFound } = this;
        const { guests, search, searchableGuests, fullSearchableGuests } = this.state;

        const guestsList = searchableGuests.length > 0 ? searchableGuests : guests
        return (
            <div>
                <h1>Guests</h1>
                {
                    (search && searchableGuests.length > 0 && searchableGuests.length !== fullSearchableGuests.length)
                    && <SearchActions addMore={addMoreFound} addRest={showAllFound} />
                }
                <br/>
                <input type="search" value={search} onChange={searchGuest}/>
                {
                    search && fullSearchableGuests.length > 0 && <span>Найдено: {fullSearchableGuests.length}</span>
                }

                <ul>
                    {
                        search && searchableGuests.length === 0
                            ? <li>Никто не подходит под поиск</li>
                            : guestsList.map(guest => (
                                <GuestCard key={guest.guest._id} guest={guest} changeStatus={changeGuestStatus} />
                            ))
                    }
                </ul>
            </div>
        );
    }
}

export default App;
