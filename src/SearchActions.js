import React, { Component } from 'react'

class SearchActions extends Component {
    render = () => {
        const { addMore, addRest } = this.props

        return (
            <div>
                <button type="button" onClick={addMore}>Еще результаты</button>
                <button type="button" onClick={addRest}>Все результаты</button>
            </div>
        )
    }
}

export default SearchActions